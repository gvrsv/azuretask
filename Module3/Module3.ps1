﻿# Module 3 task

$ResourceGroupName = 'TemplateResourceGroup1'
$Location = 'northeurope' 

New-AzureRmResourceGroup -Name $ResourceGroupName -Location $Location -Verbose
New-AzureRmResourceGroupDeployment -ResourceGroupName $ResourceGroupName `
 -TemplateUri 'https://bitbucket.org/gvrsv/azuretask/raw/master/Module3/Main_template.json' -Verbose


