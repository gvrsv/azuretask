﻿# Module 4 task

$ResourceGroupName = 'TemplateResourceGroup1'
$Location = 'northeurope' 

New-AzureRmResourceGroup -Name $ResourceGroupName -Location $Location -Verbose
New-AzureRmResourceGroupDeployment -ResourceGroupName $ResourceGroupName `
 -TemplateUri 'https://bitbucket.org/gvrsv/azuretask/raw/e688e9addea029c413e7879d4f17912ab1ab0e9d/Module3/Main_template.json' -Verbose


