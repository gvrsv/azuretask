﻿# Login to Azure account

Login-AzureRmAccount

function New-Subnet
{
    param(
    
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $ResourceGroupName,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $Location,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $VnetName,
    
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $VnetPrefix,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $GatewaySubnetName,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $GateWaySubnetPrefix,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $GatewayName,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $GWPublicIpName,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $GateWayConfigName
    )

    
    # Create a new RESOURCE GROUP
    New-AzureRmResourceGroup -Name $ResourceGroupName -Location $Location

    # configurations for gatewaysubnet
    $gwsub = New-AzureRmVirtualNetworkSubnetConfig -Name $GatewaySubnetName -AddressPrefix $GateWaySubnetPrefix

    # create VNet
    New-AzureRmVirtualNetwork -Name $VnetName -ResourceGroupName $ResourceGroupName -Location $Location -AddressPrefix $VnetPrefix -Subnet $gwsub

    # request public IP for gateway (dynamic)
    $gwip = New-AzureRmPublicIpAddress -Name $GWPublicIpName -ResourceGroupName $ResourceGroupName -Location $Location -AllocationMethod Dynamic

    # gateway configuration
    $vnet = Get-AzureRmVirtualNetwork -Name $VnetName -ResourceGroupName $ResourceGroupName
    $gwsubconf = Get-AzureRmVirtualNetworkSubnetConfig -Name $GatewaySubnetName -VirtualNetwork $vnet
    $gwipconf = New-AzureRmVirtualNetworkGatewayIpConfig -Name $GateWayConfigName -Subnet $gwsubconf -PublicIpAddress $gwip

    # create gateway
    New-AzureRmVirtualNetworkGateway -Name $GatewayName -ResourceGroupName $ResourceGroupName -Location $Location -IpConfigurations $gwipconf `
    -GatewayType Vpn -VpnType RouteBased -EnableBgp $false -GatewaySku Standard

}

# subnet parameters

$Vnet1Parameters = @{

    ResourceGroupName   = 'ResourceGroup1'
    Location            = 'westeurope'
    VnetName            = 'VNet1'
    VnetPrefix          = '10.11.0.0/16'
    GatewaySubnetName   = 'GatewaySubnet'
    GateWaySubnetPrefix = '10.11.255.0/27'
    GatewayName         = 'Gateway1'
    GWPublicIpName      = 'Gateway1_IP'
    GateWayConfigName   = 'Gateway1_Config'
}

$Vnet2Parameters = @{

    ResourceGroupName   = 'ResourceGroup2'
    Location            = 'northeurope'
    VnetName            = 'VNet2'
    VnetPrefix          = '10.12.0.0/16'
    GatewaySubnetName   = 'GatewaySubnet'
    GateWaySubnetPrefix = '10.12.255.0/27'
    GatewayName         = 'Gateway2'
    GWPublicIpName      = 'Gateway2_IP'
    GateWayConfigName   = 'Gateway2_Config'
}

# Creating Vnet1
New-Subnet @Vnet1Parameters

# Creating Vnet2
New-Subnet @Vnet2Parameters

# create connection
$SharedKey = '12QWasdf'
$Connection1 = 'connection1-to-2'
$Connection2 = 'connection2-to-1'

# get both gateways
$Vnet1Gateway = Get-AzureRmVirtualNetworkGateway -Name $Vnet1Parameters.GatewayName `
    -ResourceGroupName $Vnet1Parameters.ResourceGroupName
$Vnet2Gateway = Get-AzureRmVirtualNetworkGateway -Name $Vnet2Parameters.GatewayName `
    -ResourceGroupName $Vnet2Parameters.ResourceGroupName

# create connection between from vnet 1 to vnet 2
New-AzureRmVirtualNetworkGatewayConnection -Name $Connection1 -ResourceGroupName $Vnet1Parameters.ResourceGroupName `
-VirtualNetworkGateway1 $Vnet1Gateway -VirtualNetworkGateway2 $Vnet2Gateway -Location $Vnet1Parameters.Location `
-ConnectionType Vnet2Vnet -SharedKey $SharedKey

# create connection between from vnet 2 to vnet 1
New-AzureRmVirtualNetworkGatewayConnection -Name $Connection2 -ResourceGroupName $Vnet2Parameters.ResourceGroupName `
-VirtualNetworkGateway1 $Vnet2Gateway -VirtualNetworkGateway2 $Vnet1Gateway -Location $Vnet2Parameters.Location `
-ConnectionType Vnet2Vnet -SharedKey $SharedKey

# test VNET connection
Get-AzureRmVirtualNetworkGatewayConnection -Name $Connection1 -ResourceGroupName $Vnet1Parameters.ResourceGroupName | select -ExpandProperty ConnectionStatus
Get-AzureRmVirtualNetworkGatewayConnection -Name $Connection2 -ResourceGroupName $Vnet2Parameters.ResourceGroupName | select -ExpandProperty ConnectionStatus

